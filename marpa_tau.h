#ifndef __MARPA_TAU_H__
#define __MARPA_TAU_H__

#include "prover.h"
#include "cli.h"
#include <istream>

namespace old {
	int parse_natural3(qdb &kb, qdb &q, std::wistream &f, int &fins, string base = L"@default");
}
#endif
